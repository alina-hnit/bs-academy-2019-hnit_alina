class LoginPage {
    
    get emailInput () {return $('input[name=email]')};
    get passInput () {return $('input[type=password]')};
    get loginButton () {return $('button.button.is-primary')};
    get errorToast () {return $('div.toast.is-danger')};

};

module.exports = LoginPage;