const LoginActions = require('./actions/Login_pa')
const assert = require('assert');
const credentials = require('./../testData.json');

const pageSteps = new LoginActions();

describe('Login page tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
    });

    afterEach(() => {
        browser.reloadSession();
    });
    
    it('should not login user with invalid credentials', () => {
        
        pageSteps.enterEmail(credentials.invalidEmail);
        pageSteps.enterPassword(credentials.invalidPassword);
        pageSteps.clickLogin();
        
        pageSteps.checkForErrorToastDisplayed();
        
    });

});