class ListsPage {
    
    get listNameInput () {return $('input#list-name')};
    get saveListButton () {return $('button.button.is-success')};
    get addListButton () {return $('a.button.is-success')};
    get deleteListButton () {return $('button.button.is-danger')};
    get confirmDeleteListButton () {return $('div.buttons.is-centered button.button.is-danger')};
    get amountOfUserLists () {return $$('div.container.place-item').length};

};

module.exports = ListsPage;

