const ListsActions = require('./actions/UserLists_pa')
const MenuActions = require('../Menu/pages/menu_pa');
const assert = require('assert');
const credentials = require('./../testData.json');
const path = require('path');

const menuSteps = new MenuActions();
const pageSteps = new ListsActions();

function userLogin(email, password) {
    const emailField = $('input[name="email"]');
    const passField = $('input[type="password"]');
    const loginButton = $('button.is-primary');

    emailField.clearValue();
    emailField.setValue(email);
    passField.clearValue();
    passField.setValue(password);
    loginButton.click();
}

function waitForSpinner() {
    const spinner = $('div#preloader');
    spinner.waitForDisplayed(10000);
    spinner.waitForDisplayed(10000, true);
}

describe('Lists page tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
        userLogin(credentials.email, credentials.password);
        waitForSpinner();
        menuSteps.navigateToLists();
    });

    afterEach(() => {
        browser.reloadSession();
    });
    
    it('create a list', () => {

        const listCount1 = pageSteps.getAmountOfUserLists(); 
        
        pageSteps.navigateToCreateListPage();
        
        pageSteps.enterListName('MyList');

        pageSteps.saveList();
        
        browser.refresh();
        
        assert.strictEqual(listCount1, pageSteps.getAmountOfUserLists()-1);
        
    });
    
    it('delete a list', () => {
            
        const listCount1 = pageSteps.getAmountOfUserLists();  

        pageSteps.deleteList();   
        
        browser.refresh();
        
        assert.strictEqual(listCount1, pageSteps.getAmountOfUserLists()+1);    
         
    });

});