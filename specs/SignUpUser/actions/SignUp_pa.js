const SignUpPage = require('../page/SignUp_po');
const page = new SignUpPage();

class SignUpActions {

    navigateToSignUpPage() {
        page.signUpLink.waitForDisplayed(2000);
        page.signUpLink.click();
    }
    
    enterFirstName(value) {
        page.firstNameInput.waitForDisplayed(2000);
        page.firstNameInput.clearValue();
        page.firstNameInput.setValue(value);
    }
    
    enterLastName(value) {
        page.lastNameInput.waitForDisplayed(2000);
        page.lastNameInput.clearValue();
        page.lastNameInput.setValue(value);
    }
    
    enterEmail(value) {
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.clearValue();
        page.emailInput.setValue(value);
    }
    
    enterPassword(value) {
        page.passInput.waitForDisplayed(2000);
        page.passInput.clearValue();
        page.passInput.setValue(value);
    }
    
    createUser() {
        page.createButton.waitForDisplayed(2000);
        page.createButton.click();
    }
    
    getCurrentUrl() {
        page.signUpLink.waitForDisplayed(5000);
        return browser.getUrl(); 
    }
}

module.exports = SignUpActions;
