const SignUpActions = require('./actions/SignUp_pa')
const assert = require('assert');
const credentials = require('./../testData.json');
const randomEmail = require('random-email');

const pageSteps = new SignUpActions();

describe('Register new user tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
    });

    afterEach(() => {
        browser.reloadSession();
    });
    
    it('register new user', () => {
            
        pageSteps.navigateToSignUpPage();
        
        pageSteps.enterFirstName(credentials.testFirstName);
        pageSteps.enterLastName(credentials.testLastName);
        pageSteps.enterEmail(randomEmail({ domain: 'examples.com' }));
        pageSteps.enterPassword(credentials.newUserPassword);
        
        pageSteps.createUser();
        
        assert.strictEqual(pageSteps.getCurrentUrl(), "https://165.227.137.250/login");
        
    });

});