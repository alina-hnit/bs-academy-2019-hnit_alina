class SignUpPage {
    
    get signUpLink () {return $('a.link.link-signup')};
    get firstNameInput () {return $('input[name=firstName]')};
    get lastNameInput () {return $('input[name=lastName]')};
    get emailInput () {return $('input[name=email]')};
    get passInput () {return $('input[type=password]')};
    get createButton () {return $('button.button.is-primary')};

};

module.exports = SignUpPage;

