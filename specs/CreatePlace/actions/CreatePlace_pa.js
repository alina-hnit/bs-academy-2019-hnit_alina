const CreatePlacePage = require('../page/CreatePlace_po');
const page = new CreatePlacePage();
const path = require('path');
const ROOT_DIR = path.resolve(__dirname);

class CreatePlaceActions {

    enterPlaceName(value) {
        page.placeName.waitForDisplayed(2000);
        page.placeName.clearValue();
        page.placeName.setValue(value);
    }
    
    enterPlaceLocation(value) {
        page.placeLocation.waitForDisplayed(2000);
        page.placeLocation.clearValue();
        page.placeLocation.setValue(value);
        page.placeLocationDropdown.waitForDisplayed(2000);
        page.placeLocationDropdown.click();
    }
    
    enterPlaceZip(value) {
        page.placeZip.waitForDisplayed(2000);
        page.placeZip.clearValue();
        page.placeZip.setValue(value);
    }
    
    enterPlaceAddress(value) {
        page.placeAddress.waitForDisplayed(2000);
        page.placeAddress.clearValue();
        page.placeAddress.setValue(value);
    }
    
    enterPlacePhone(value) {
        page.placePhone.waitForDisplayed(2000);
        page.placePhone.clearValue();
        page.placePhone.setValue(value);
    }
    
    enterPlaceWebsite(value) {
        page.placeWebsite.waitForDisplayed(2000);
        page.placeWebsite.clearValue();
        page.placeWebsite.setValue(value);
    }
    
    enterPlaceDescription(value) {
        page.placeDescription.waitForDisplayed(2000);
        page.placeDescription.clearValue();
        page.placeDescription.setValue(value);
    }
    
    goToNextPage() {
        page.nextButton.click();
    }
    
    uploadPhotos(imgPath) {
        
        //page.photoUploadArea.waitForDisplayed(5000);
        const filePath=path.join(__dirname, imgPath);

        const remoteFilePath=browser.uploadFile(filePath);

        browser.pause(3000);

        page.photoUploadArea.setValue(remoteFilePath);
        //page.photoUploadArea.setValue(filePath);
    }
}

module.exports = CreatePlaceActions;
