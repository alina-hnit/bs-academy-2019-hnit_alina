class CreatePlacePage {

    
    get placeName () { return $('input[placeholder="the Best place"]')};
    get placeLocation () { return $('div.field-body input[placeholder="Location"]')};
    get placeLocationDropdown () { return $('b')};
    get placeZip () { return $('input[placeholder="09678"]')};
    get placeAddress () { return $('input[placeholder="Khreschatyk St., 14"]')};
    get placePhone () { return $('input[placeholder="+380961112233"]')};
    get placeWebsite () { return $('input[placeholder="http://the-best-place.com/"]')};
    get placeDescription () { return $('textarea.textarea')};
    get nextButton () { return $('span.button.is-success')};
    get photoUploadArea () { return $('input[type="file"]')};

};

module.exports = CreatePlacePage;