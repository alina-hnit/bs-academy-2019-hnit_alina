const assert = require('assert');
const randomEmail = require('random-email');
const path = require('path');

describe('webdriver.io page', () => {
        
    it('register new user on Hedonist', () => {
        
        browser.url('https://staging.bsa-hedonist.online/signup');
        
        const firstNameField = $('input[name=firstName]');
        const lastNameField = $('input[name=lastName]');
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const createButton = $('button.button.is-primary');
        const signupLink = $('a.link.link-signup');
        
        createButton.waitForDisplayed(2000);
        
        const email = randomEmail({ domain: 'example.com' });
        
        firstNameField.setValue('TestFirstName');
        lastNameField.setValue('TestLastName');
        emailField.setValue(email);
        passField.setValue('M5b7Q9$la');
        createButton.click();
        
        signupLink.waitForDisplayed(3000);
        
        const actualURL = browser.getUrl();
        
        assert.equal(actualURL, "https://staging.bsa-hedonist.online/login");
        
    });
    
    it('should not login user with invalid credentials', () => {
        
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const errorToast = $('div.toast.is-danger');
        
        loginButton.waitForDisplayed(1000);
        
        emailField.setValue('notvalid');
        passField.setValue('inval');
        loginButton.click();
        
        errorToast.waitForDisplayed(3000);
        
    });
    
    xit('should create a place', () => {
        
        browser.url('https://staging.bsa-hedonist.online/login');
        
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const mapContainer = $('div.mapboxgl-canvas-container');
        const placeName = $('input[placeholder="the Best place"]');
        const placeLocation = $('div div div div div div div div div div div input[placeholder="Location"]');
        const placeZip = $('input[placeholder="09678"]');
        const placeAddress = $('input[placeholder="Khreschatyk St., 14"]');
        const placePhone = $('input[placeholder="+380961112233"]');
        const placeWebsite = $('input[placeholder="http://the-best-place.com/"]');
        const placeDescription = $('textarea.textarea');
        const nextButton = $('span.button.is-success');
        
        loginButton.waitForDisplayed(1000);
        
        emailField.setValue('alfael@ukr.net');
        passField.setValue('Welcome');
        loginButton.click();
        
        mapContainer.waitForDisplayed(5000);
        
        browser.url('https://staging.bsa-hedonist.online/places/add');
        
        placeName.setValue('Meet & Burger');
        placeLocation.clearValue();
        placeLocation.setValue('Lviv');
        placeZip.setValue('79005');
        placeAddress.setValue('12 Hnatiuka str.');
        placePhone.setValue('+380980104201');
        placeWebsite.setValue('http://www.meatandburger.com.ua/');
        placeDescription.setValue('So much thought goes into creating each one of these unique masterpieces that when it comes to the table you don’t know if you should admire and photograph it, or just stuff it straight into your mouth. We’ll opt for the latter…');
        nextButton.click();
        
        browser.pause(3000);
        
        const photoUploadArea = $('input[type=file]');
        const filePath = ('/img/mb5.jpg');
        const remoteFilePath = browser.uploadFile('/img/mb5.jpg');
        browser.pause(3000);
        photoUploadArea.setValue(remoteFilePath);
        
        
        
        

        
        
        
        browser.pause(10000);
        
        assert.equal();
    });
    
    it('should create a place', () => {
        
        browser.url('https://staging.bsa-hedonist.online/login');
        
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const mapContainer = $('div.mapboxgl-canvas-container');
        const addListButton = $('a.button.is-success');
        const listName = $('input#list-name');
        const saveListButton = $('button.button.is-success');
        let listCount1;
        let listCount2;
        
        loginButton.waitForDisplayed(1000);
        
        emailField.setValue('alfael@ukr.net');
        passField.setValue('Welcome');
        loginButton.click();
        
        mapContainer.waitForDisplayed(3000);
        
        browser.url('https://staging.bsa-hedonist.online/my-lists');
        
        browser.pause(2000);
        
        listCount1 = $$('div.container.place-item').length;
        
        addListButton.click();
        
        listName.waitForDisplayed(3000);
        
        listName.setValue('MyList');
        saveListButton.click();       
            
        browser.pause(2000);
        
        listCount2 = $$('div.container.place-item').length;
        
        assert.equal(listCount1+1, listCount2);
        
    });
    
    it('delete a place', () => {
        
        //browser.url('https://staging.bsa-hedonist.online/my-lists');
        
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const mapContainer = $('div.mapboxgl-canvas-container');
        const deleteListButton = $('button.button.is-danger');
        const confirmDeleteListButton = $('div.buttons.is-centered button.button.is-danger');
        let listCount1;
        let listCount2;
        
        //browser.pause(2000);
        
        listCount1 = $$('div.container.place-item').length;
        
        deleteListButton.click();
        
        confirmDeleteListButton.waitForDisplayed(3000);
        
        confirmDeleteListButton.click();
        
        browser.pause(2000);
        
        listCount2 = $$('div.container.place-item').length;
        
        assert.equal(listCount1, listCount2+1);    
        
        
    });
});